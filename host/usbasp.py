#!/usr/bin/env python3
# @author Mehdi MAAREF

import usb
from usb import *
from usb.legacy import *
import usb.control as control
import threading 
from array import array

CMD_CONNECT	 = 1
CMD_DISCONNECT	 = 2
CMD_BUFFER_READ	 = 3
CMD_BUFFER_WRITE = 4

def getHandle(_idVendor = 0x16c0, _idProduct = 0x05dc):
    for bus in usb.busses():
        devices = bus.devices
        for dev in devices:
            if dev.idVendor == _idVendor and dev.idProduct==_idProduct:
                return UsbASP(dev)
    return None

class UsbASP(object):
  def __init__(self, _dev):
    self.dev = _dev.open()
  ''' Main commands '''
  def ctrlsend(self, cmd, buffer, val=0, idx=0):
    return self.dev.controlMsg(TYPE_VENDOR | RECIP_DEVICE | ENDPOINT_OUT, cmd, buffer, val, idx, 500)
  def ctrlrecv(self,cmd, length=0, val=0, idx=0):
    return self.dev.controlMsg(TYPE_VENDOR | RECIP_DEVICE | ENDPOINT_IN, cmd, length, val, idx)
  def usb_connect(self):
    return self.ctrlrecv(CMD_CONNECT, 10)
  def usb_disconnect(self):
    return self.ctrlrecv(CMD_DISCONNECT, 10)
  ''' setup CMD read write '''
  def read_buffer(self, length = 256, idx=0):
    return self.ctrlrecv(CMD_BUFFER_READ, length, 0, idx)
  def write_buffer(self, buffer):
    resp = array('B')
    for i in range(0,len(buffer),4):
      v = buffer[i:i+4]+b'   '
      resp += self.ctrlrecv(CMD_BUFFER_WRITE, 4, v[1]<<8 | v[0], v[3]<<8 | v[2])
    return resp.tobytes()

if __name__ == "__main__":
  u = getHandle()
  r=u.ctrlrecv(CMD_CONNECT, 8, 9, 10)
  print(r)
  r = u.usb_connect()
  #r=u.ctrlrecv(CMD_CONNECT, 8, 9, 10)
