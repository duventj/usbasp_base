#!/usr/bin/env python3
# @author Mehdi MAAREF

import time
import sys
import threading
import getopt
from cmd import Cmd
from usbasp import *
from array import array
import logging

logging.basicConfig(level=logging.DEBUG,format='(%(threadName)-10s) %(message)s')

def u16toStr(tab):
  r=''
  for i in range(0,len(tab)):
    r+= bytes.fromhex(hex(tab[i])[2:]).decode('utf-8')
  return r
def uchar2u16(r0, r1):
  return r0 | r1<<8

class UsbaspPrompt(Cmd):
  def __init__(self):
    super().__init__()
    self.prompt = '(Usbasp) '
    self.device = getHandle()
    if self.device is None:
      raise Exception("Can't find USB device..\n")
  ''' Usefull commands '''
  def emptyline(self):
    pass
  def do_shell(self, cmd):
    try:
      self.res = os.system(cmd)
    except Exception as e:
      print(e)
  def help_exit(self):
    print('Exit the command prompt')
  def do_exit(self, arg=''):
    self.do_disconnect()
    return not print('')
  def do_EOF(self, line='') : 
    return self.do_exit('')
  ''' Init commands '''
  def do_connect(self, args=''):
    r=self.device.usb_connect()
    print(r.tobytes())
  def do_disconnect(self, args=''):
    r=self.device.usb_disconnect()
    print(r.tobytes())
  ''' Read commands '''
  def do_read_buffer(self, args=''):
    print(self.device.read_buffer().tobytes())
  ''' Write commands '''
  def do_write_buffer(self, args):
    args=args.encode('utf-8')
    r=self.device.write_buffer(args)
  ''' Test commands '''
  def do_echo(self, args):
    if not args :
      args='Hello Atmega!'
    self.do_write_buffer(args)
    self.do_read_buffer()
  def do_start_daemon(self, args):
    pass

if __name__ == "__main__":
  try:
    p = UsbaspPrompt()
    p.cmdloop()
  except KeyboardInterrupt:
    p.do_exit()
    print('\nBye Bye...')
  except Exception as e:
    print('Error :', e)
    try: 
      p.do_exit()
    except Exception as e:
      pass
    
    