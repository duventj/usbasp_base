#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "usbasp.h"
#include "usbdrv.h"
#include "clock.h"

/** globals **/
static int8_t i, j;

uchar dev_state = 0;

uchar buffer[256]; 
uchar buffer_start = 0;
uchar buffer_end = 0;

inline void usb_loop() {
    usbPoll();
    toggleLedGreen();
    toggleLedRed();
}

int main(void) {
  /* no pullups on USB and ISP pins */
  PORTD = 0;
  PORTB = 0;
  /* all outputs except PD2 = INT0 */
  DDRD = ~(1 << 2);
  
  /* output SE0 for USB reset */
  DDRB = ~0;
  /* USB Reset by device only required on Watchdog Reset -> little delay*/
  i = j = 0;
  while (--j) 
    while(--i);
  i = j = 0;
  /* all USB and ISP pins inputs */
  DDRB = 0;
  
  /* all inputs except PC0, PC1 */
  DDRC = 0x03;
  PORTC = 0xfe;
  
  /* init timer */
  clockInit();
  /* main event loop */
  usbInit();
  //mirf_init();
  sei();
  for (;;) {
      usb_loop();
  }
  return 0;
}

