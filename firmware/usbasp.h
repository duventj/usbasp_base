#ifndef USBASP_H_
#define USBASP_H_

#define BUFFER_LEN 64

/* USB setup commands */
#define CMD_CONNECT	   1
#define CMD_DISCONNECT 	   2
#define CMD_BUFFER_READ	   3
#define CMD_BUFFER_WRITE   4

/* device state */
#define STATE_IDLE      0
#define STATE_USB       1

#define setDevState(_state)    dev_state |= 1<<_state
#define unsetDevState(_state)  dev_state &= (0xff & 0<<_state)
#define isDevState(_state)    (dev_state & 1<<_state)

/* macros for gpio functions */
#define ledRedOn()    PORTC &= ~(1 << PC1)
#define ledRedOff()   PORTC |= (1 << PC1)
#define ledGreenOn()  PORTC &= ~(1 << PC0)
#define ledGreenOff() PORTC |= (1 << PC0)

#define isLedRedOff()   (PORTC & (1 << PC1))
#define isLedRedOn()    ~(isLedRedOff())
#define isLedGreenOff() (PORTC & (1 << PC0))
#define isLedGreenOn()  ~(isLedGreenOff())

#define toggleLedRed()  PORTC ^= (1 << PC1)
#define toggleLedGreen()  PORTC ^= (1 << PC0)


#endif /* USBASP_H_ */
