#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "usbasp.h"
#include "usbdrv.h"

#define HELLO_STR       "hello"
#define HELLO_STR_LEN   5

extern uchar dev_state;
extern uchar buffer[];
extern uchar buffer_start;
extern uchar buffer_end;


uchar usbFunctionSetup(uchar data[8]) {
  uchar ret=0;
  
  switch(data[1]){
    case CMD_CONNECT:
      setDevState(STATE_USB);
      usbMsgPtr = ((uchar*)HELLO_STR);
      ret = HELLO_STR_LEN;
      break;
    case CMD_DISCONNECT:
      unsetDevState(STATE_USB);
      usbMsgPtr = ((uchar*)"bye bye");
      ret = 7;
      break;
    case CMD_BUFFER_READ:
      usbMsgPtr = &(buffer[buffer_start]);
      ret = (buffer_end >= buffer_start) ? (buffer_end-buffer_start) : 256-buffer_start;
      buffer_start += ret;
      break;
    case CMD_BUFFER_WRITE:
      buffer[buffer_end++] = data[2]; //rq->wValue.bytes[0];
      buffer[buffer_end++] = data[3]; //rq->wValue.bytes[1];
      buffer[buffer_end++] = data[4]; //rq->wIndex.bytes[0];
      buffer[buffer_end++] = data[5]; //rq->wIndex.bytes[1];
      usbMsgPtr = &buffer_end;
      ret = 1;
    default:
      break;
  }
  return ret;
}
